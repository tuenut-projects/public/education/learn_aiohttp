import argparse
from pathlib import Path

from pydantic import BaseModel, IPvAnyAddress


BASE_DIR = Path(__file__).parent


class StartAppArgs(BaseModel):
    host: IPvAnyAddress
    port: int


def parse_args() -> StartAppArgs:
    parser = argparse.ArgumentParser(description="Example TODO service.")
    parser.add_argument("--host", default="127.0.0.1")
    parser.add_argument("--port", default=28088)

    args = parser.parse_args()

    return StartAppArgs.model_validate(args.__dict__)
