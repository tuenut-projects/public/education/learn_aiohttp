import aiohttp_jinja2
import jinja2
from aiohttp import web

from learn_aiohttp.config import parse_args, BASE_DIR
from learn_aiohttp.libs.logging import get_logger
from learn_aiohttp.app.services.csrf import csrf_context
from learn_aiohttp.app.middlewares.csrf import csrf_middleware
from learn_aiohttp.app.middlewares.request_id import request_id_middleware
from learn_aiohttp.app.urls import routes


def get_app():
    logger = get_logger()

    middlewares = [request_id_middleware, csrf_middleware]
    contexts = [csrf_context]

    app = web.Application(middlewares=middlewares, logger=logger)
    app.add_routes(routes)
    app.cleanup_ctx.extend(contexts)

    aiohttp_jinja2.setup(
        app, loader=jinja2.FileSystemLoader(BASE_DIR / "app/templates")
    )

    return app


def run_app():
    args = parse_args()
    app = get_app()

    web.run_app(app, host=str(args.host), port=args.port)


if __name__ == "__main__":
    run_app()
