from aiohttp import web

from learn_aiohttp.app.utils.response import ResponseSchema, prepare_response


async def index_view(request: web.Request) -> web.Response:
    response_data = ResponseSchema(result="Hello there!")

    return prepare_response(request, response_data, "index.jinja2")
