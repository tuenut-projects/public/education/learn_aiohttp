from aiohttp import web

from learn_aiohttp.libs.logging import get_logger
from learn_aiohttp.app.utils.response import prepare_response


async def login_view(request: web.Request) -> web.Response:
    logger = get_logger(request)

    if request.method == "GET":
        return prepare_response(request, None, "login.jinja2")
    elif request.method == "POST":
        data = await request.post()
        await logger.adebug(f"Request body: {data}")

        return web.Response(text="OK")


async def logout_view(request: web.Request):
    location = request.app.router['index'].url_for()
    raise web.HTTPFound(location=location)


async def register_view(request: web.Request) -> web.Response:
    logger = get_logger(request)

    if request.method == "GET":
        return prepare_response(request, None, "register.jinja2")
    elif request.method == "POST":
        data = await request.post()
        await logger.adebug(f"Request body: {data}")

        return web.Response(text="OK")
