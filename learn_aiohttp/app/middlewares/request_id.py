from typing import Callable, Awaitable
from uuid import uuid4

from aiohttp import web

from learn_aiohttp.libs.logging import get_logger, request_logger


@web.middleware
async def request_id_middleware(
    request: web.Request,
    handler: Callable[[web.Request], Awaitable[web.Response]],
):
    logger = set_logger(request)

    await logger.adebug(
        "Receive request",
        request_content=await request.text(),
        request_headers=dict(request.headers),
        request_method=request.method,
    )

    return await handler(request)


def set_logger(request: web.Request):
    request["id"] = str(uuid4())

    logger = get_logger(request).bind(
        request_id=request["id"],
        request_path=request.path,
        request_external_id=request["id"],
    )
    request_logger.set(logger)
    request["logger"] = logger

    return logger
