from typing import Callable, Awaitable

from aiohttp import web

from learn_aiohttp.libs.csrf import AbcCsrfStorage, SAFE_METHODS
from learn_aiohttp.libs.logging import get_logger


@web.middleware
async def csrf_middleware(
    request: web.Request,
    handler: Callable[[web.Request], Awaitable[web.Response]],
):
    logger = get_logger(request)

    csrf_storage: AbcCsrfStorage = request.app["csrf"]
    is_valid, token = await csrf_storage.check(request)

    if request.method in SAFE_METHODS and not is_valid:
        await logger.awarning(
            f"User make [{request.method}] request to <{request.path}> "
            f"with invalid CSRF token."
        )
        raise web.HTTPUnauthorized(reason="Invalid CSRF token.")

    try:
        response = await handler(request)
    finally:
        await csrf_storage.delete(token)

    new_token = await csrf_storage.set(response)

    await logger.adebug(f"Send new token to client: <{new_token}>.")

    return response
