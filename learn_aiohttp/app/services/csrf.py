from aiohttp import web

from learn_aiohttp.libs.csrf import (
    CSRF_COOKIE,
    CSRF_HEADER,
    CsrfCookiePolicy,
    CsrfHeaderPolicy,
    InMemoryCsrfStorage,
)


async def csrf_context(app: web.Application):
    policies = [CsrfCookiePolicy(CSRF_COOKIE), CsrfHeaderPolicy(CSRF_HEADER)]
    storage = await InMemoryCsrfStorage.open(policies)

    app["csrf"] = storage

    yield

    await storage.close()
