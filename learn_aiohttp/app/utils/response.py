from typing import Generic, TypeVar

from aiohttp import web
from aiohttp_jinja2 import render_template
from pydantic import BaseModel


T = TypeVar("T")


class ResponseSchema(BaseModel, Generic[T]):
    result: T


def prepare_response(
    request: web.Request,
    response_data: ResponseSchema = None,
    template: str = None,
) -> web.Response:
    if response_data:
        context = response_data.model_dump()
    else:
        context = {}

    if template and "text/html" in request.headers.get("Accept", "").lower():
        return render_template(template, request, context=context)
    else:
        return web.json_response(context)
