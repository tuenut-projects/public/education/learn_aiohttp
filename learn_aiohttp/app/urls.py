from aiohttp import web
from .views.index import index_view
from .views.auth import login_view, logout_view, register_view


routes = [
    web.get("/", index_view),
    web.get("/login", login_view),
    web.post("/login", login_view),
    web.delete("/logout", logout_view),
    web.get("/register", register_view),
    web.post("/register", register_view),
]
