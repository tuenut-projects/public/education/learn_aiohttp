import abc
from uuid import uuid4

from aiohttp import web
from pydantic import BaseModel, UUID4, Field


CSRF_HEADER = "X-Csrf-Token"
CSRF_COOKIE = "csrf_token"
SAFE_METHODS = ["POST", "PUT", "PATCH", "DELETE"]


class AbcPolicy(abc.ABC):
    def __init__(self, token_key: str):
        self.token_key = token_key

    @abc.abstractmethod
    async def get(self, request: web.Request) -> str | None:
        ...

    @abc.abstractmethod
    async def set(self, response: web.Response, token: str):
        ...


class CsrfCookiePolicy(AbcPolicy):
    async def get(self, request: web.Request) -> str | None:
        return request.cookies.get(self.token_key)

    async def set(self, response: web.Response, token: str):
        response.set_cookie(self.token_key, token)


class CsrfHeaderPolicy(AbcPolicy):
    async def get(self, request: web.Request) -> str | None:
        return request.headers.get(self.token_key)

    async def set(self, response: web.Response, token: str):
        response.headers[self.token_key] = token


class CsrfToken(BaseModel):
    token: UUID4 | None = Field(default_factory=uuid4)
    user: str = "Anonymous"

    def __str__(self):
        return str(self.token)


class AbcCsrfStorage(abc.ABC):
    def __init__(self, policies: list[AbcPolicy]):
        self.policies = policies

    @classmethod
    @abc.abstractmethod
    async def open(cls, *args, **kwargs):
        """Creating storage, open connections, etc"""
        ...

    @abc.abstractmethod
    async def close(self):
        ...

    async def get(self, token: str) -> CsrfToken:
        """Get token from storage by request or create new"""
        ...

    @abc.abstractmethod
    async def store(self, token: CsrfToken) -> str:
        """Store token into storage"""
        ...

    @abc.abstractmethod
    async def delete(self, token: CsrfToken):
        """Remove token from storage"""
        ...

    async def set(self, response: web.Response) -> CsrfToken:
        token = CsrfToken()
        raw_token = await self.store(token)

        for csrf_token_policy in self.policies:
            await csrf_token_policy.set(response, raw_token)

        return token

    async def check(self, request: web.Request) -> tuple[bool, CsrfToken]:
        """Check token in storage"""
        request_token = None

        for csrf_token_policy in self.policies:
            request_token = await csrf_token_policy.get(request)
            if request_token:
                break

        token = CsrfToken(token=request_token)
        return await self.get(request_token) == token, token


class InMemoryCsrfStorage(AbcCsrfStorage):
    storage: dict[str, CsrfToken]

    @classmethod
    async def open(cls, *args, **kwargs):
        return cls(*args, **kwargs)

    async def close(self):
        del self.storage

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.storage = {}

    async def get(self, token: str) -> CsrfToken:
        return self.storage.get(token, CsrfToken(token=None))

    async def store(self, token: CsrfToken) -> str:
        key = str(token.token)
        self.storage[key] = token
        return key

    async def delete(self, token: CsrfToken):
        key = str(token.token)
        try:
            self.storage.pop(key)
        except:
            pass
