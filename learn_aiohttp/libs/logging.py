from contextvars import ContextVar
from logging.config import dictConfig

import structlog
from aiohttp import web


def get_logger(
    request: web.Request = None, app: web.Application = None
) -> structlog.stdlib.BoundLogger:
    try:
        return request_logger.get()
    except LookupError:
        pass

    if request:
        logger = request.app.logger
    elif app:
        logger = app.logger
    else:
        logger = structlog.get_logger()

    return logger


LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "json": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.processors.JSONRenderer(),
        },
        "plaintext": {
            "()": structlog.stdlib.ProcessorFormatter,
            "processor": structlog.dev.ConsoleRenderer(),
        },
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "plaintext"},
        "json_file": {
            "class": "logging.handlers.WatchedFileHandler",
            "filename": ".logs/app.jsonl",
            "formatter": "json",
        },
    },
    "loggers": {
        "aiohttp": {
            "propagate": False,
            "handlers": ["console", "json_file"],
            "level": "DEBUG",
        },
        "aiomysql": {
            "propagate": False,
            "handlers": ["console", "json_file"],
            "level": "DEBUG",
        },
        "root": {"handlers": ["console", "json_file"], "level": "DEBUG"},
    },
}


def configure_logging():
    dictConfig(LOGGING)

    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.contextvars.merge_contextvars,
            structlog.stdlib.add_log_level,
            structlog.processors.TimeStamper("iso", key="@timestamp"),
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.CallsiteParameterAdder(
                {
                    structlog.processors.CallsiteParameter.FILENAME,
                    structlog.processors.CallsiteParameter.FUNC_NAME,
                    structlog.processors.CallsiteParameter.LINENO,
                    structlog.processors.CallsiteParameter.MODULE,
                }
            ),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        wrapper_class=structlog.stdlib.BoundLogger,
        logger_factory=structlog.stdlib.LoggerFactory(),
        cache_logger_on_first_use=True,
    )


request_logger = ContextVar("request_logger")
